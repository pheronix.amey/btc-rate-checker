aws_region = "ap-south-1"


vpc_name                 = "Azul Project VPC"
cluster_name             = "Azul-Cluster"
vpc_cidr                 = "10.0.0.0/16"
vpc_private_subnets      = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
vpc_public_subnets       = ["10.0.10.0/24", "10.0.20.0/24", "10.0.30.0/24"]
vpc_database_subnets     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
vpc_enable_nat_gateway   = true
vpc_single_nat_gateway   = true
vpc_enable_dns_hostnames = true


rds_sg_name              = "RDS-SG"
rds_db_identifier        = "btcdb"
rds_mysql_engine_version = "8.0.35"
rds_instance_type        = "db.t3.micro"
rds_instance_storage     = 20
rds_db_name              = "btcdb"
rds_username             = "am3y"
rds_port                 = 3306













