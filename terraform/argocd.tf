resource "kubernetes_namespace" "argocd" {  
  metadata {
    name = "argocd"
  }
  depends_on = [ module.eks ]
}

resource "helm_release" "argocd" {
  count      = var.argocd_enabled ? 1 : 0
  name       = "argocd"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  version    = "6.7.2"
  namespace  = kubernetes_namespace.argocd.metadata.0.name

  set {
    name  = "server.extraArgs[0]"
    value = "--insecure"
  }

  set {
    name  = "server.extraArgs[1]"
    value = "--rootpath=/argocd"
  }

  depends_on = [
    kubernetes_namespace.argocd
  ]
}




resource "kubernetes_manifest" "azul_argo_application" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "Application"
    metadata = {
      name      = "azul-argo-application"
      namespace = "argocd"
    }
    spec = {
      project = "default"

      source = {
        repoURL        = "https://gitlab.com/pheronix.amey/btc-rate-checker-k8s-manifests.git"
        targetRevision = "HEAD"
        path           = "kubernetes"
      }

      destination = {
        server    = "https://kubernetes.default.svc"
        namespace = "default"
      }

      syncPolicy = {
        syncOptions = [
          "CreateNamespace=true"
        ]

        automated = {
          selfHeal = true
          prune    = true
        }
      }
    }
  }
  count      = var.argocd_enabled ? 1 : 0
  depends_on = [ helm_release.argocd ]
}


resource "kubernetes_manifest" "argo_application" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "Application"
    metadata = {
      name      = "argo-application"
      namespace = "argocd"
    }
    spec = {
      project = "default"

      source = {
        repoURL        = "https://gitlab.com/pheronix.amey/btc-rate-checker-k8s-manifests.git"
        targetRevision = "HEAD"
        path           = "argocd"
      }

      destination = {
        server    = "https://kubernetes.default.svc"
        namespace = "argocd"
      }

      syncPolicy = {
        syncOptions = [
          "CreateNamespace=true"
        ]

        automated = {
          selfHeal = true
          prune    = true
        }
      }
    }
  }
  count      = var.argocd_enabled ? 1 : 0
  depends_on = [ helm_release.argocd ]
}