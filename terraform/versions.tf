terraform {
  required_version = ">= 1.7.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.31"
    }
  }


  backend "s3" {
    bucket = "terraform-backend-am3y"
    key    = "backend/terraform.tfstate"
    region = "ap-south-1"

    dynamodb_table = "terraform-backend-dynamodtable-am3y"
  }
}