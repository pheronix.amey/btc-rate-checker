from flask import Flask, render_template, request
import boto3
from datetime import datetime
import json

app = Flask(__name__)

bucket_name = 'terraform-backend-am3y'
folder_name = 'temp/'

def fetch_rates_from_s3():
    s3 = boto3.client('s3')
    info_list = []

    response = s3.list_objects_v2(Bucket=bucket_name, Prefix=folder_name)
    for item in response.get('Contents', []):
        filename = item['Key']
        
        if filename == folder_name:
            continue
        
        file_content = s3.get_object(Bucket=bucket_name, Key=filename)['Body'].read().decode('utf-8')
        info_map = extract_info(filename, file_content)
        info_list.append(info_map)

    return info_list

def extract_info(filename, content):
    parts = filename.split('/')[-1].split('_')
    date_part = parts[1]
    time_part = parts[2].split('.')[0]
    timestamp_str = f"{date_part}_{time_part}"
    timestamp = datetime.strptime(timestamp_str, "%Y%m%d_%H%M%S")
    data = json.loads(content)
    czk_rate = round(float(data['data']['rates']['CZK']), 3)

    
    return {
        "date": timestamp.strftime("%Y-%m-%d"),
        "time": timestamp.strftime("%H:%M:%S"),
        "rate": czk_rate
    }

@app.route('/', methods=['GET', 'POST'])
def index():
    rates = fetch_rates_from_s3()
    selected_date = request.form.get('date') if request.method == 'POST' else None
    filtered_rates = [rate for rate in rates if rate['date'] == selected_date] if selected_date else rates
    return render_template('index.html', rates=filtered_rates, selected_date=selected_date)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
